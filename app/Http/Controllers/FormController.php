<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('halaman.biodata');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $firstnama = $request->firstnama;
        $lastnama = $request->lastnama;
        $biodata = $request->bio;

        return view('halaman.home', compact('firstnama', 'lastnama', 'biodata'));
    }
}
